﻿var COLOR_OK = "#88ff88";
var COLOR_FAIL = "#ff8888";
var BORDER_DEFAULT = "0.1em solid transparent";
var BORDER_SELECTED = "0.1em solid black";
var size = 2;
var blindMode = true;
var starttime;
var selected = null;

function onLoad() {
    var hash = getHash();
    if (hash != null) {
        size = parseInt(hash);
    }
    size = Math.max(2, size);

    restartWithSize(size);

    updateTime();
    setInterval(updateTime, 300);
}

function getTime() {
    return new Date().getTime();
}

function updateTime() {
	$("#elapsed").text(getElapsedSeconds() + " seconds");
}

function restartWithSize(newSize) {
	selected = null;
    setHash(newSize);
    size = newSize;
    createCells();
    showCellText();
    starttime = getTime();
}

function createCells() {
    $("table").hide();

    var table = document.getElementsByTagName("table")[0];
    table.innerHTML = "";

    for (var i=0; i < size; i++) {
        var tr = document.createElement("tr");
        table.appendChild(tr);

        for (var j=0; j < size; j++) {
            var td = document.createElement("td");
            tr.appendChild(td);

            var id = "c" + (i * size) + j;
            td.setAttribute("id", id);
            td.setAttribute("draggable", true);
            td.setAttribute("ondragstart", "onDragStart(event)");
            td.setAttribute("ondragenter", "onDragEnter(event)");
            td.setAttribute("ondragover", "onDragOver(event)");
            td.setAttribute("ondragleave", "onDragLeave(event)");
            td.setAttribute("ondrop", "onDrop(event)");
            $("#"+id).click(onClick);
        }
    }
    shuffleCells();

    $("table").fadeIn(500);
}

function getCells() {
    return document.getElementsByTagName("td");
}

function shuffleCells() {
    var numbers = createNumberArray(size*size);
    shuffleArray(numbers);

    var cells = getCells();
    for (var i=0; i < cells.length; i++) {
        cells[i].textContent = numbers[i];
    }

    if (checkWin()) {
        shuffleCells();
    } else {
        colorCells();
    }
}

function swap(id1, id2) {
    var node1 = document.getElementById(id1);
    var node2 = document.getElementById(id2);

    var tmp = node1.textContent;
    node1.textContent = node2.textContent;
    node2.textContent = tmp;

    colorCells();

    if (checkWin()) {
        var time = getElapsedSeconds();
        var noobModeText = (blindMode) ? "" : " in noob mode";
        document.getElementById("info").innerHTML +=
            "<br/>" + size + "x" + size + ": " + time + " seconds" + noobModeText;

        restartWithSize(size + 1);
    }
}

function checkWin() {
    var cells = getCells();
    var win = true;
    for (var i=0; i < cells.length; i++) {
        var content = cells[i].textContent;
        if (content != (i+1)) {
            win = false;
            break;
        }
    }
    return win;
}

function colorCells() {
    var cells = getCells();
    for (var i=0; i < cells.length; i++) {
        var color = COLOR_OK;
        if (cells[i].textContent != (i+1)) {
            color = COLOR_FAIL;
        }
        cells[i].style.backgroundColor = color;
        cells[i].style.border = BORDER_DEFAULT;
    }
}

function showCellText() {
    setCellTextColor("black");
}

function hideCellText() {
    setCellTextColor("transparent");
}

function setCellTextColor(color) {
    var cells = getCells();
    for (var i=0; i < cells.length; i++) {
        cells[i].style.color = color;
    }
}

function setCellBorder(node, color) {
    node.style.border = "0.1em solid " + color;
}

function getElapsedSeconds() {
    var elapsed = (getTime() - starttime) / 1000;
    return Math.round(elapsed);
}

//-----------------------------------------------------------------------------
// Event Handlers
//-----------------------------------------------------------------------------

function onRestart() {
    document.getElementById("info").innerHTML = "";
    restartWithSize(2);
}

function onBlind() {
    blindMode = !blindMode;
    $("#btnBlind").text((blindMode) ? "Noob mode" : "Blind mode");
    restartWithSize(size);
}

function onReveal() {
    showCellText();
}

function onDragStart(event) {
    if (blindMode) {
        hideCellText();
    }
    event.dataTransfer.setData("id", event.target.id);
    setCellBorder(event.target, "white");
}

function onClick() {
	if (blindMode) {
		hideCellText();
	}
	var id = $(this).attr("id");
	if (selected == null) {
		selected = id;
		$(this).css("border", BORDER_SELECTED);
	} else {
		swap(id, selected);
		selected = null;
	}
}

function onDragEnter(event) {
    setCellBorder(event.target, "black");
}

function onDragOver(event) {
    event.preventDefault();
}

function onDragLeave(event) {
    setCellBorder(event.target, "transparent");
}

function onDrop(event) {
    event.preventDefault();
    setCellBorder(event.target, "transparent");
    var id1 = event.dataTransfer.getData("id");
    var id2 = event.target.id;
    swap(id1, id2);
}

//-----------------------------------------------------------------------------
// Common
//-----------------------------------------------------------------------------

function getRandom(min, max) {
    var canBeNegative = min < 0;
    var isNegative = canBeNegative &&
        Math.random() < 0.5;

    var result;
    if (isNegative) {
        result = 0 - Math.floor(Math.random() * min + 1);
    }
    else {
        result = Math.floor(Math.random() * (max - min + 1) + min);
    }

    return result;
}

function createNumberArray(max) {
    var arr = [];
    for (var i=1; i <= max; i++) {
        arr.push(i);
    }
    return arr;
}

function shuffleArray(a) {
    for (var i = a.length; i; i--) {
        var j = Math.floor(Math.random() * i);
        var x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function getHash() {
    var hash = window.location.hash;
    return (hash.length > 0) ? hash.substring(1) : null;
}

function setHash(value) {
    window.location.hash = value;
}